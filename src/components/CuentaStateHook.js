import React , {useState} from 'react';


export default function SimpleStateFunction(){
    const [cuenta, setCuenta] = useState(0);

    return (
        <div>
            La cuenta es: {cuenta}

            <button onClick={()=> setCuenta( cuenta + 1)}>Aumentar cuenta</button>
        </div>
    )
}