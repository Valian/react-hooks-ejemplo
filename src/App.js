import React from 'react';
import logo from './logo.svg';
import './App.css';
import SimpleStateFunction from "./components/CuentaStateHook.js";
import Coordenadas from "./components/PositionComponentHook";

function App() {
  return (
    <div className="App">
        <SimpleStateFunction/>
        <Coordenadas/>
    </div>
  );
}

export default App;
